<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\StatusesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('/auth')->group(function() {
   Route::post('/sign-in', [AuthController::class, 'login'])
       ->name('auth.login');
   Route::post('/sign-up', [AuthController::class, 'register'])
       ->name('auth.register');
});

Route::middleware('auth:sanctum')->group(function() {
    Route::prefix('rest-api')->group(function() {
        // Route::get('/') -> index route for aggregate all data

        Route::resource('statuses', StatusesController::class)
            ->only([
                'index',
                'show',
                'store',
                'update',
                'destroy'
            ]);
    });
});
