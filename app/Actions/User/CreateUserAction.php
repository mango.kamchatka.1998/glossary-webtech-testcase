<?php

namespace App\Actions\User;

use App\Actions\Crud\CreateAction;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

final class CreateUserAction extends CreateAction {

    /**
     * @var bool Use hooks ?
     */
    protected static bool $hooks = true;

    /**
     * @var string|null Model class
     */
    protected static ?string $model = User::class;

    /**
     * @param array $data
     * @param Model|Builder $instance
     * @return void
     */
    protected function before(array& $data, Model|Builder $instance): void {
        $data['password'] = Hash::make($data['password']);
    }
}
