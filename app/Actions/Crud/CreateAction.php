<?php

namespace App\Actions\Crud;

use App\Actions\AbstractAction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Throwable;

abstract class CreateAction extends AbstractAction {
    /**
     * @param array $data
     * @return Model|Builder
     * @throws Throwable
     */
    public function handle(array $data): Model|Builder {
        $instance = $this->getModel();

        throw_if(!$this->authorized($data, $instance), "Unauthorized access");

        if(static::$hooks) {
            $this->before($data, $instance);
        }

        $instance = $instance->create($data);

        if(static::$hooks) {
            $this->after($data, $instance);
        }

        return $instance;
    }
}
