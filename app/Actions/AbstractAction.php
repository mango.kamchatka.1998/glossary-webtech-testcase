<?php

namespace App\Actions;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Throwable;

/**
 * That mechanism should to use DTO objects for
 * properly usage(types, etc..)
 */
abstract class AbstractAction implements IAction {

    /**
     * @var string|null Required parameter for take model
     */
    protected static ?string $model = null;

    /**
     * @var bool Optional parameter, indicates need to call hooks or not
     */
    protected static bool $hooks = true;

    /**
     * Get builder for model class
     * @return Model|Builder
     * @throws Throwable
     */
    protected function getModel(): Model|Builder {
        throw_if(empty(static::$model));

        return static::$model::query();
    }

    /**
     * Find one model and returns builder
     * @param int $id
     * @return Model|Builder
     * @throws Throwable
     */
    protected function find(int $id): Model|Builder {
        return $this->getModel()->findOrFail($id);
    }

    /**
     * Lifecycle hook, called before handle
     * @param array $data
     * @param Model|Builder $instance
     * @return void
     */
    protected function before(array& $data, Model|Builder $instance): void
    { }

    /**
     * Lifecycle hook, called after handle
     * @param array $data
     * @param Model|Builder $instance
     * @return void
     */
    protected function after(array& $data, Model|Builder $instance): void
    { }

    /**
     * @param array $data
     * @param Model|Builder $instance
     * @return bool
     */
    protected function authorized(array $data, Model|Builder $instance): bool {
        return true;
    }
}
