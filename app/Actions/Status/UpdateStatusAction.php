<?php

namespace App\Actions\Status;

use App\Actions\Crud\UpdateAction;
use App\Models\Statuses;

final class UpdateStatusAction extends UpdateAction {
    protected static ?string $model = Statuses::class;
    protected static bool $hooks = false;
}
