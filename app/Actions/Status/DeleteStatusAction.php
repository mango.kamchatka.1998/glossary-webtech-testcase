<?php

namespace App\Actions\Status;

use App\Actions\Crud\DeleteAction;
use App\Models\Statuses;

final class DeleteStatusAction extends DeleteAction {
    protected static ?string $model = Statuses::class;
    protected static bool $hooks = true;
}
