<?php

namespace App\Actions\Status;

use App\Actions\Crud\CreateAction;
use App\Models\Statuses;

final class CreateStatusAction extends CreateAction {
    protected static ?string $model = Statuses::class;
    protected static bool $hooks = false;
}
