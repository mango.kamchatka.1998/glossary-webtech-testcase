<?php

namespace App\Http\Controllers;

use App\Http\Requests\Status\CreateStatusRequest;
use App\Http\Requests\Status\UpdateStatusRequest;
use App\Http\Resources\StatusResource;
use App\Models\Statuses;
use App\Services\Entities\StatusesService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

final class StatusesController extends Controller {
    public function __construct(
        private StatusesService $service
    ) { }

    public function index(): ResourceCollection {
        return $this->service->getIndexPage();
    }

    /**
     * @param CreateStatusRequest $request
     * @return JsonResource
     * @throws \Throwable
     */
    public function store(CreateStatusRequest $request): JsonResource {
        return $this->service->create(...$request->validated());
    }

    /**
     * @param Statuses $statuses
     * @return JsonResource
     */
    public function show(Statuses $statuses): JsonResource {
        return StatusResource::make($statuses);
    }

    /**
     * @param UpdateStatusRequest $request
     * @param int $id
     * @return JsonResource
     * @throws \Throwable
     */
    public function update(UpdateStatusRequest $request, int $id): JsonResource {
        return $this->service->update($id, ...$request->validated());
    }

    /**
     * @param int $id
     * @return JsonResource
     * @throws \Throwable
     */
    public function destroy(int $id): JsonResource {
        return $this->service->delete($id);
    }
}
