<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Services\Auth\Abstract\ILogin;
use App\Services\Auth\Abstract\IRegister;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * That controller contains service container realization
 * See AuthServiceProvider::boot()
 */
class AuthController extends Controller {

    /**
     * @OA\Post(
     *   path="/auth/login",
     *   tags={"Auth"},
     *   summary="Sanctum Login",
     *   description="Login a user and generate token",
     *   operationId="authLoginSanctum",
     *   @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
     *               type="object",
     *               @OA\Property(
     *                   property="email",
     *                   description="User email",
     *                   type="string",
     *               ),
     *               @OA\Property(
     *                   property="password",
     *                   description="User password",
     *                   type="string",
     *               ),
     *           )
     *       )
     *   ),
     *  @OA\Response(
     *         response="200",
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                      @OA\Property(
     *                          property="email",
     *                          type="string"
     *                      ),
     *                      @OA\Property(
     *                          property="login",
     *                          type="string"
     *                      ),
     *                      @OA\Property(
     *                          property="token",
     *                          type="string"
     *                      ),
     *                     example={
     *                         "email": "someuser@mail.com",
     *                         "login": "Some User",
     *                         "token": "encrypted_token_here"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *   @OA\Response(response="403",description="Password was invalid"),
     * )
     */
    public function login(
        LoginRequest $request,
        ILogin $login
    ): JsonResource {
        return $login->login(...$request->validated());
    }

    public function register(
        RegisterRequest $request,
        IRegister $register
    ): JsonResource {
        return $register->register(...$request->validated());
    }
}
