<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\StreamedResponse;

final class DecryptApiTokens
{
    public function handle(Request $request, Closure $next): Response|RedirectResponse|JsonResponse|StreamedResponse
    {
        $token = $request->bearerToken();

        if($token) {
            try {
                $request->headers->replace([
                    'Authorization' => 'Bearer ' . base64_decode(Crypt::decrypt($token))
                ]);
            } catch(\Exception $e) {
                Log::error($e);
                abort(403);
            }
        }

        return $next($request);
    }
}
