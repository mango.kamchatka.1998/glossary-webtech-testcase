<?php

namespace App\Services\Entities;

use App\Actions\Status\CreateStatusAction;
use App\Actions\Status\DeleteStatusAction;
use App\Actions\Status\UpdateStatusAction;
use App\Http\Resources\StatusResource;
use App\Http\Resources\SuccessfullyDeletedResource;
use App\Repositories\StatusesRepository;
use Illuminate\Http\Resources\Json\ResourceCollection;

final class StatusesService {
    /**
     * @param StatusesRepository $repository
     */
    public function __construct(
        private readonly StatusesRepository $repository,
    ) { }

    /**
     * @return ResourceCollection
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getIndexPage(): ResourceCollection
    {
        return StatusResource::collection($this->repository->paginate());
    }

    /**
     * @param string $name
     * @param string $color
     * @return StatusResource
     * @throws \Throwable
     */
    public function create(
        string $name,
        string $color
    ): StatusResource
    {
        return StatusResource::make(
            (new CreateStatusAction())->handle([
                'name' => $name,
                'color' => $color
            ])
        );
    }

    /**
     * @param int $id
     * @param string|null $name
     * @param string|null $color
     * @return StatusResource
     * @throws \Throwable
     */
    public function update(
        int $id,
        ?string $name = null,
        ?string $color = null
    ): StatusResource
    {
        return StatusResource::make(
            (new UpdateStatusAction())->handle([
                'id' => $id,
                'name' => $name,
                'color' => $color
            ])
        );
    }

    /**
     * @param int $id
     * @return SuccessfullyDeletedResource
     * @throws \Throwable
     */
    public function delete(int $id): SuccessfullyDeletedResource
    {
        return SuccessfullyDeletedResource::make(
            (new DeleteStatusAction())->handle(['id' => $id])
        );
    }
}
