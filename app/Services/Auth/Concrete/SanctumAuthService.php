<?php

namespace App\Services\Auth\Concrete;

use App\Actions\User\CreateUserAction;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\Auth\Abstract\ILogin;
use App\Services\Auth\Abstract\IRegister;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Throwable;

final class SanctumAuthService implements ILogin, IRegister {
    public function __construct(
        protected UserRepository $userRepository
    ) { }

    public function login(string $email, string $password): JsonResource {
        $user = $this->userRepository->checkPassword(
            'email',
            $email,
            $password
        );

        if(!$user) {
            abort(403, 'Password was invalid');
        }

        return $this->response($user);
    }

    /**
     * @throws Throwable
     */
    public function register(
        string $name,
        string $email,
        string $password
    ): JsonResource {
        $user = (new CreateUserAction())->handle([
            'name' => $name,
            'email' => $email,
            'password' => $password
        ]);

        return UserResource::make($user);
    }

    /**
     * @param Model|Builder|User $user
     * @return UserResource
     */
    private function response(Model|Builder|User $user): UserResource {
        return UserResource::make(collect([
            'email' => $user->email,
            'login' => $user->login,
            'token' => $this->userRepository->token($user)
        ]));
    }
}
