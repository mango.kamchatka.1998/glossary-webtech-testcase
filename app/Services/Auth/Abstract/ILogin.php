<?php

namespace App\Services\Auth\Abstract;

use Illuminate\Http\Resources\Json\JsonResource;

interface ILogin {
    public function login(string $email, string $password): JsonResource;
}
