<?php

namespace App\Services\Auth\Abstract;

use Illuminate\Http\Resources\Json\JsonResource;

interface IRegister {
    public function register(
        string $name,
        string $email,
        string $password
    ): JsonResource;
}
