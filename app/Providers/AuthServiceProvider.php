<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Services\Auth\Abstract\ILogin;
use App\Services\Auth\Abstract\IRegister;
use App\Services\Auth\Concrete\SanctumAuthService;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->app->bind(ILogin::class, SanctumAuthService::class);
        $this->app->bind(IRegister::class, SanctumAuthService::class);
    }
}
