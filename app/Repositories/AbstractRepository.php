<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class AbstractRepository implements IRepository {

    /**
     * @var int Default page
     */
    protected static int $page = 1;

    /**
     * @var int Default records per page
     */
    protected static int $records = 10;

    /**
     * @var array
     */
    protected static array $relations = [];

    /**
     * @param Builder $builder
     * @return LengthAwarePaginator
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function applyPagination(Builder $builder): LengthAwarePaginator {
        $withRelations = $this->load($builder);

        return $this->filters($withRelations)->paginate(
            request()->input('records', static::$records),
            page: request()->input('page', static::$page)
        );
    }

    /**
     * @param Model|Builder $builder
     * @return Builder
     */
    protected function load(Model|Builder $builder): Builder {
        return $builder->with(static::$relations);
    }

    /**
     * @param Builder $builder
     * @return Builder
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function filters(Builder $builder): Builder {
        if(request()->has('filters')) {
            foreach (request()->get('filters') as $key => $value) {
                $builder = $builder->where($key, $value);
            }
        }

        if(request()->has('search') && request()->has('search-by')) {
            $search = request()->get('search');
            $by = request()->get('search-by');
            $builder = $builder->where($by, 'LIKE', '%' . $search . '%');
        }

        return $builder;
    }

    /**
     * @return LengthAwarePaginator
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function paginate(): LengthAwarePaginator {
        return $this->applyPagination($this->query());
    }
}
