<?php

namespace App\Repositories;

use App\Models\Statuses;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

final class StatusesRepository extends AbstractRepository {
    public function __construct(
        private readonly Statuses $statuses
    ) { }

    public function query(): Model|Builder {
        return $this->statuses->newQuery();
    }
}
