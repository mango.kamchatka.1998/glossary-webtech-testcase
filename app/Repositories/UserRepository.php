<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

final class UserRepository extends AbstractRepository {
    /**
     * @param User $user
     */
    public function __construct(
        protected User $user
    ) { }

    /**
     * @param string $by
     * @param string $value
     * @param string $password
     * @return ?User
     */
    public function checkPassword(string $by, string $value, string $password): ?User {
        $user = $this->get($by, $value);
        $success = Hash::check(
            $password,
            $this->get($by, $value)->password
        );

        return $success ? $user : null;
    }

    /**
     * @param string $by
     * @param string $value
     * @return User|Model|Builder
     */
    public function get(string $by, string $value): User|Model|Builder {
        return $this->query()->where($by, $value)->firstOrFail();
    }

    /**
     * @param User $user
     * @return string
     */
    public function token(User $user): string {
        return Crypt::encrypt(
            base64_encode($user->createToken(Str::random(36))->plainTextToken)
        );
    }

    /**
     * @return User|Model|Builder
     */
    public function query(): User|Model|Builder {
        return $this->user->newQuery();
    }
}
