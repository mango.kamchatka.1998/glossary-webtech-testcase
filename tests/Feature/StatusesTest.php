<?php

namespace Tests\Feature;

use App\Models\Statuses;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StatusesTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_create_status(): void
    {
        $user = User::factory()->create()->first();

        $response = $this->actingAs($user)->post('api/rest-api/statuses', data: [
            'name' => 'first status',
            'color' => 'red'
        ]);

        $response->assertStatus(201); // FOR RESOURCE CREATION
        $this->assertDatabaseHas(Statuses::class, [
            'name' => 'first status',
            'color' => 'red'
        ]);
    }
}
